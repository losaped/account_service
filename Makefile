gen:
	swagger generate server --exclude-main -A AccountService -t . -f ./swagger/swagger.yml
	openapi2proto -spec ./swagger/swagger.yml -out=./proto/account-service.proto
	protoc --go_out=plugins=grpc:./rpc/gen proto/account-service.proto

curl -X POST -H "Content-Type: application/json" -d '{"email": "some@email", "password": "somepass"} http://localhost:40093/create