package main

import (
	"bitbucket.org/losaped/account_service/http/models"
	"bitbucket.org/losaped/account_service/http/restapi"
	"bitbucket.org/losaped/account_service/http/restapi/operations"
	"bitbucket.org/losaped/account_service/rpc"
	"bitbucket.org/losaped/account_service/service"
	"bitbucket.org/losaped/account_service/store"
	"bitbucket.org/losaped/account_service/store/inmemory"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"

	log "github.com/sirupsen/logrus"
)

func main() {
	mailSender := rpc.NewMailSender("localhost:3131")

	svc := service.Service{
		Accounts:   inmemory.NewAccountsStore(),
		MailSender: mailSender,
	}

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalf("unable load swagger spec %v", err)
	}

	api := operations.NewAccountServiceAPI(swaggerSpec)
	apiServer := restapi.NewServer(api)
	defer func() {
		_ = apiServer.Shutdown()
	}()

	api.CreateAccountHandler = operations.CreateAccountHandlerFunc(
		func(params operations.CreateAccountParams) middleware.Responder {
			acc := store.Account{
				Email:    swag.StringValue(params.Body.Email),
				Password: swag.StringValue(params.Body.Password),
			}

			newAcc, err := svc.CreateAccount(acc)
			if err != nil {
				return operations.NewCreateAccountDefault(500).WithPayload(&models.Error{
					Code:    500,
					Message: swag.String(err.Error()),
				})
			}

			return operations.NewCreateAccountCreated().WithPayload(&models.Account{
				Email: newAcc.Email,
				ID:    newAcc.ID,
				Phone: newAcc.Phone,
			})
		},
	)

	apiServer.ConfigureAPI()
	if err := apiServer.Serve(); err != nil {
		log.Fatal(err)
	}
}
