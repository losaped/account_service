package rpc

import (
	"context"
	"fmt"
	"time"

	mailsvc "bitbucket.org/losaped/mail-service"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

type MailSender struct {
	addr string
}

func (cli *MailSender) Send(from, to, subj, body string) error {
	fmt.Println("ebanarot")
	log.Info("ebanarot")
	conn, err := cli.createConn()
	if err != nil {
		fmt.Println("error on create connection", err)
		return err
	}

	defer conn.Close()

	fmt.Println(conn.GetState())
	rpcCli := mailsvc.NewMailServiceClient(conn)
	req := mailsvc.SendMessageRequest{
		From:    from,
		To:      to,
		Subject: subj,
		Body:    body,
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	log.Info("send message")
	fmt.Println("send message")

	empt, err := rpcCli.Send(ctx, &req)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("empty returned")
	log.Info("Empty returned", empt)
	return err
}

func NewMailSender(addr string) *MailSender {
	cli := &MailSender{
		addr: addr,
	}

	return cli
}

func (cli *MailSender) createConn() (*grpc.ClientConn, error) {
	return grpc.Dial(cli.addr,
		grpc.WithInsecure(),
		grpc.WithTimeout(5*time.Second),
		grpc.WithBlock(),
	)
}
