package service

import (
	"bitbucket.org/losaped/account_service/store"
	"github.com/sirupsen/logrus"
)

type Sender interface {
	Send(from, to, subj, body string) error
}

type Service struct {
	Accounts   store.Accounts
	MailSender Sender
}

func (s Service) CreateAccount(account store.Account) (*store.Account, error) {
	// acc, err := s.Accounts.Create(account)
	// if err != nil {
	// 	return nil, err
	// }

	logrus.WithFields(logrus.Fields{"some_field": "lalala"}).Info("trying to send message")

	if err := s.MailSender.Send("from@noreply.com", account.Email, "account confirmation", "enter code"); err != nil {
		return nil, err
	}

	acc := &store.Account{Email: "losaped@gmail.com"}
	return acc, nil
}

func (s Service) DeleteAccount(id string) error {
	return s.Accounts.Delete(id)
}
