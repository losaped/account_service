package store

type Account struct {
	ID        string
	Email     string
	Phone     string
	Password  string
	Confirmed bool
}

// Accounts interface that concrete store should implement
type Accounts interface {
	Create(acc Account) (*Account, error)
	Delete(ID string) error
}
