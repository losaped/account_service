package inmemory

import (
	"strconv"
	"sync"

	"bitbucket.org/losaped/account_service/store"
)

type Accounts struct {
	accounts map[string]*store.Account
	mu       sync.Mutex
}

func NewAccountsStore() *Accounts {
	return &Accounts{
		accounts: make(map[string]*store.Account),
	}
}

func (store *Accounts) Create(acc store.Account) (*store.Account, error) {
	store.mu.Lock()
	defer store.mu.Unlock()

	acc.ID = strconv.Itoa(len(store.accounts) + 1)
	store.accounts[acc.ID] = &acc
	return &acc, nil
}

func (store *Accounts) Delete(id string) error {
	store.mu.Lock()
	store.mu.Unlock()

	delete(store.accounts, id)
	return nil
}
